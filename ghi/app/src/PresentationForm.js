import React, { useEffect, useState } from 'react';

const PresentationForm = () => {
  const [conferences, setConferences] = useState([]);
  const [presenterName, setPresenterName] = useState('');
  const [presenterEmail, setPresenterEmail] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [title, setTitle] = useState('');
  const [synopsis, setSynopsis] = useState('');
  const [selectedConferenceId, setSelectedConferenceId] = useState('');

  useEffect(() => {
    const fetchConferences = async () => {
      try {
        const response = await fetch('http://localhost:8000/api/conferences/');
        if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences);
        } else {
          console.error('Failed to fetch conferences:', response.status);
        }
      } catch (error) {
        console.error('Failed to fetch conferences:', error);
      }
    };

    fetchConferences();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();

    const presentationData = {
      presenter_name: presenterName,
      presenter_email: presenterEmail,
      company_name: companyName,
      title,
      synopsis,
    };

    try {
      const response = await fetch(
        `http://localhost:8000${selectedConferenceId}presentations/`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(presentationData),
        }
      );

      if (response.ok) {
        setPresenterName('');
        setPresenterEmail('');
        setCompanyName('');
        setTitle('');
        setSynopsis('');
        setSelectedConferenceId('');
        console.log('Presentation submitted successfully!');
      } else {
        console.error('Failed to submit presentation:', response.status);
      }
    } catch (error) {
      console.error('Failed to submit presentation:', error);
    }
  };

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  placeholder="Presenter name"
                  required
                  type="text"
                  name="presenter_name"
                  id="presenter_name"
                  className="form-control"
                  value={presenterName}
                  onChange={(e) => setPresenterName(e.target.value)}
                />
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Presenter email"
                  required
                  type="email"
                  name="presenter_email"
                  id="presenter_email"
                  className="form-control"
                  value={presenterEmail}
                  onChange={(e) => setPresenterEmail(e.target.value)}
                />
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Company name"
                  type="text"
                  name="company_name"
                  id="company_name"
                  className="form-control"
                  value={companyName}
                  onChange={(e) => setCompanyName(e.target.value)}
                />
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Title"
                  required
                  type="text"
                  name="title"
                  id="title"
                  className="form-control"
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea
                  className="form-control"
                  id="synopsis"
                  rows="3"
                  name="synopsis"
                  value={synopsis}
                  onChange={(e) => setSynopsis(e.target.value)}
                ></textarea>
              </div>
              <div className="mb-3">
                <select
                  required
                  name="conference"
                  id="conference"
                  className="form-select"
                  value={selectedConferenceId}
                  onChange={(e) => setSelectedConferenceId(e.target.value)}
                >
                  <option value="">Choose a conference</option>
                  {conferences.map((conference) => (
                    <option key={conference.href} value={conference.href}>
                      {conference.name}
                    </option>
                  ))}
                </select>
              </div>
              <button type="submit" className="btn btn-primary">
                Create
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PresentationForm;
