function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
  const formattedStartDate = new Date(startDate).toLocaleDateString();
  const formattedEndDate = new Date(endDate).toLocaleDateString();
  return `
    <div class="card shadow mb-3">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer">${formattedStartDate}-${formattedEndDate}</div>
        </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);
    let index = 0;
    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const locationName = details.conference.location.name;
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = details.conference.starts;
          const endDate = details.conference.ends;
          const html = createCard(title, description, pictureUrl, startDate, endDate, locationName);
          const columnDiv = document.querySelector(`#col-${index}`);
            columnDiv.innerHTML += html;
            index+=1;
            if (index > 2) {
                index = 0;
                }
        }
      }

    }
  } catch (e) {
    console.error(e)
    // Figure out what to do if an error is raised
  }

});
